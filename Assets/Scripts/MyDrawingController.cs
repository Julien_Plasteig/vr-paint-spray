﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDrawingController : MonoBehaviour {

    public SpotDrawer pinSpot;
    public Drawable[] drawables;

    //public Transform rotatingBuddha;
    public float rotateAngle = 0f;
    
	// Update is called once per frame
	void Update () {


        if (Input.GetMouseButton(0))
        {
            Debug.Log("Peinture !!!");
            pinSpot.UpdateDrawingMat();
            foreach (var drawable in drawables)
                pinSpot.Draw(drawable);
        }
	}
}
